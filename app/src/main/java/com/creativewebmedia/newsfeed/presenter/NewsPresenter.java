package com.creativewebmedia.newsfeed.presenter;

/**
 * News feed presenter interface.
 */
public interface NewsPresenter {

    /**
     * Reloading news feed from server
     */
    void onReloadData();

    /**
     * Destroying screen of news
     */
    void onDestroy();
}

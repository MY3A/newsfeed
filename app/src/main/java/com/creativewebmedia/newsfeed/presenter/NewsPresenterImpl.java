package com.creativewebmedia.newsfeed.presenter;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.creativewebmedia.newsfeed.data.api.WebServiceManager;
import com.creativewebmedia.newsfeed.data.database.DBManager;
import com.creativewebmedia.newsfeed.data.model.NewsFeedWrapper;
import com.creativewebmedia.newsfeed.data.model.NewsItem;
import com.creativewebmedia.newsfeed.view.NewsView;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Implementation for NewsPresenter interface
 */
public class NewsPresenterImpl implements NewsPresenter, LoaderManager.LoaderCallbacks<Cursor> {

    private static final int LOADER_NEWS_FEED = 0;

    private ArrayList<NewsItem> mNewsFeedData = new ArrayList<>();
    private DBManager mDBManagerManager;
    private NewsView mNewsView;
    private LoaderManager mLoaderManager;
    private Context mContext;

    public NewsPresenterImpl(NewsView view, LoaderManager loaderManager, Context context) {
        mLoaderManager = loaderManager;
        mContext = context;
        mDBManagerManager = new DBManager(context);
        mDBManagerManager.open();
        mNewsView = view;
        mLoaderManager.restartLoader(LOADER_NEWS_FEED, null, this);
    }

    @Override
    public void onReloadData() {

        mNewsFeedData.clear();
        mDBManagerManager.clearData();
        mNewsView.showProgressDialog(true);

        WebServiceManager.getNewsFeed(new Callback<NewsFeedWrapper>() {
            @Override
            public void success(NewsFeedWrapper newsFeedWrapper, Response response) {
                mNewsView.showProgressDialog(false);

                if (newsFeedWrapper != null) {
                    mNewsFeedData = newsFeedWrapper.getNewsItems();
                    mDBManagerManager.insertNews(mNewsFeedData);
                    mLoaderManager.restartLoader(LOADER_NEWS_FEED, null, NewsPresenterImpl.this);

                } else
                    mNewsView.showErrorDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                mNewsView.showErrorDialog();
            }
        });

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bndl) {
        switch (id) {
            case LOADER_NEWS_FEED:
                return new NewsCursorLoader(mContext, mDBManagerManager, new NewsCursorLoader.ReloadListener() {
                    @Override
                    public void onEmptyData() {
                        onReloadData();
                    }
                });
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()) {
            case LOADER_NEWS_FEED:
                mNewsView.setItems(cursor);
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

        switch (loader.getId()) {
            case LOADER_NEWS_FEED:
                mNewsView.setItems(null);
                break;
        }
    }

    /**
     * CursorLoader class for receiving data from local database
     */
    static class NewsCursorLoader extends CursorLoader {

        DBManager mDbManager;
        ReloadListener mReloadListener;

        public NewsCursorLoader(Context context, DBManager dbManager, ReloadListener reloadListener) {
            super(context);
            this.mDbManager = dbManager;
            this.mReloadListener = reloadListener;
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = mDbManager.getNewsFeed();
            if (cursor != null && cursor.getCount() == 0 && mReloadListener != null)
                mReloadListener.onEmptyData();

            return mDbManager.getNewsFeed();
        }

        /**
         * Interface for case of empty data
         */
        public interface ReloadListener {
            void onEmptyData();
        }
    }

    @Override
    public void onDestroy() {
        mDBManagerManager.close();
    }
}

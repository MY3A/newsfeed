package com.creativewebmedia.newsfeed.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.creativewebmedia.newsfeed.R;
import com.creativewebmedia.newsfeed.Utils.Constants;
import com.squareup.picasso.Picasso;

/**
 * Adapter for RecyclerView based on Cursor
 */
public class SimpleNewsCursorAdapter extends RecyclerViewCursorAdapter<SimpleNewsCursorAdapter.FeedListRowHolder> {

    private final LayoutInflater mLayoutInflater;
    private OnItemClickListener mOnItemClickListener;

    public SimpleNewsCursorAdapter(final Context context, OnItemClickListener itemClickListener) {
        super();
        this.mOnItemClickListener = itemClickListener;
        this.mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public FeedListRowHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = this.mLayoutInflater.inflate(R.layout.news_list_item, parent, false);
        return new FeedListRowHolder(view);
    }

    @Override
    public void onBindViewHolder(final FeedListRowHolder feedListRowHolder, final Cursor cursor) {

        feedListRowHolder.mText.setText(cursor.getString(cursor.getColumnIndex(Constants.COLUMN_TITLE)));
        feedListRowHolder.date.setText(cursor.getString(cursor.getColumnIndex(Constants.COLUMN_DATE)));

        Picasso.with(feedListRowHolder.itemView.getContext())
                .load(cursor.getString(cursor.getColumnIndex(Constants.COLUMN_IMG)))
                .into(feedListRowHolder.image);

        final String url = cursor.getString(cursor.getColumnIndex(Constants.COLUMN_URL));
        feedListRowHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null)
                    mOnItemClickListener.onItemClicked(url);
            }
        });
    }

    /**
     * View holder for Recycler view data
     */
    public static class FeedListRowHolder extends RecyclerView.ViewHolder {
        protected TextView mText;
        protected ImageView image;
        protected TextView date;

        public FeedListRowHolder(View view) {
            super(view);
            this.mText = (TextView) view.findViewById(R.id.title);
            this.image = (ImageView) view.findViewById(R.id.image);
            this.date = (TextView) view.findViewById(R.id.date);
        }
    }

    /**
     * Interface for item click implementation
     */
    public interface OnItemClickListener {
        void onItemClicked(String url);
    }
}

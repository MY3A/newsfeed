package com.creativewebmedia.newsfeed.adapters;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;

/**
 * Base abstarct class for RecyclerView adapter based on Cursor
 *
 * @param <VH> view holder
 */
public abstract class RecyclerViewCursorAdapter<VH extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<VH> {
    private Cursor cursor;

    /**
     * setting cursor to adapter
     *
     * @param cursor cursor with data
     */
    public void swapCursor(final Cursor cursor) {
        this.cursor = cursor;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return this.cursor != null
                ? this.cursor.getCount()
                : 0;
    }

    /**
     * Getting item from cursor
     *
     * @param position position number of item
     * @return cursor, moved to position
     */
    public Cursor getItem(final int position) {
        if (this.cursor != null && !this.cursor.isClosed()) {
            this.cursor.moveToPosition(position);
        }

        return this.cursor;
    }

    /**
     * Getting current adapter cursor
     *
     * @return cursor with data
     */
    public Cursor getCursor() {
        return this.cursor;
    }

    @Override
    public final void onBindViewHolder(final VH holder, final int position) {
        final Cursor cursor = this.getItem(position);
        this.onBindViewHolder(holder, cursor);
    }

    /**
     * Base method for binding data with view holder
     *
     * @param holder view holder
     * @param cursor cursor with data
     */
    public abstract void onBindViewHolder(final VH holder, final Cursor cursor);
}
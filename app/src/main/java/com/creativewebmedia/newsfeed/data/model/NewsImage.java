package com.creativewebmedia.newsfeed.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class for news item image information.
 */
public class NewsImage {

    @SerializedName("Photo")
    String mPhoto;

    @SerializedName("Thumb")
    String mThumb;

    @SerializedName("PhotoCaption")
    String mPhotoCaption;

    public String getPhoto() {
        return mPhoto;
    }

    public void setPhoto(String photo) {
        mPhoto = photo;
    }

    public String getThumb() {
        return mThumb;
    }

    public void setThumb(String thumb) {
        mThumb = thumb;
    }

    public String getPhotoCaption() {
        return mPhotoCaption;
    }

    public void setPhotoCaption(String photoCaption) {
        mPhotoCaption = photoCaption;
    }
}

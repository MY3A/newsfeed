package com.creativewebmedia.newsfeed.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Class for news feed parsing
 */
public class NewsFeedWrapper {

    @SerializedName("Pagination")
    Pagination mPagination;

    @SerializedName("NewsItem")
    ArrayList<NewsItem> mNewsItems;

    public Pagination getPagination() {
        return mPagination;
    }

    public void setPagination(Pagination pagination) {
        mPagination = pagination;
    }

    public ArrayList<NewsItem> getNewsItems() {
        return mNewsItems;
    }

    public void setNewsItems(ArrayList<NewsItem> newsItems) {
        mNewsItems = newsItems;
    }
}

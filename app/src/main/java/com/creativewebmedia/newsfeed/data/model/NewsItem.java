package com.creativewebmedia.newsfeed.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Class for single news item.
 */
public class NewsItem {

    @SerializedName("NewsItemId")
    long mId;

    @SerializedName("HeadLine")
    String mHeadline;

    @SerializedName("Agency")
    String mAgancy;

    @SerializedName("DateLine")
    String mDate;

    @SerializedName("WebURL")
    String mUrl;

    @SerializedName("Caption")
    String mCaption;

    @SerializedName("Image")
    NewsImage mImage;

    @SerializedName("video")
    ArrayList<NewsVideo> mVideos;

    @SerializedName("Keywords")
    String mKeywords;

    @SerializedName("Story")
    String mStory;

    @SerializedName("CommentCountUrl")
    String mCommentCountUrl;

    @SerializedName("CommentFeedUrl")
    String mCommentFeedUrl;

    @SerializedName("Related")
    String mRelatedLink;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getHeadline() {
        return mHeadline;
    }

    public void setHeadline(String headline) {
        mHeadline = headline;
    }

    public String getAgancy() {
        return mAgancy;
    }

    public void setAgancy(String agancy) {
        mAgancy = agancy;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getCaption() {
        return mCaption;
    }

    public void setCaption(String caption) {
        mCaption = caption;
    }

    public NewsImage getImage() {
        return mImage;
    }

    public void setImage(NewsImage image) {
        mImage = image;
    }

    public ArrayList<NewsVideo> getVideos() {
        return mVideos;
    }

    public void setVideos(ArrayList<NewsVideo> videos) {
        mVideos = videos;
    }

    public String getKeywords() {
        return mKeywords;
    }

    public void setKeywords(String keywords) {
        mKeywords = keywords;
    }

    public String getStory() {
        return mStory;
    }

    public void setStory(String story) {
        mStory = story;
    }

    public String getCommentCountUrl() {
        return mCommentCountUrl;
    }

    public void setCommentCountUrl(String commentCountUrl) {
        mCommentCountUrl = commentCountUrl;
    }

    public String getCommentFeedUrl() {
        return mCommentFeedUrl;
    }

    public void setCommentFeedUrl(String commentFeedUrl) {
        mCommentFeedUrl = commentFeedUrl;
    }

    public String getRelatedLink() {
        return mRelatedLink;
    }

    public void setRelatedLink(String relatedLink) {
        mRelatedLink = relatedLink;
    }
}


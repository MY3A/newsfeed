package com.creativewebmedia.newsfeed.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class for Pagination item parsing.
 */
public class Pagination {

    @SerializedName("TotalPages")
    int mTotalPages;

    @SerializedName("PageNo")
    int mPageNo;

    @SerializedName("PerPage")
    int mPerPage;

    @SerializedName("WebURL")
    String mWebUrl;

    public int getTotalPages() {
        return mTotalPages;
    }

    public void setTotalPages(int totalPages) {
        mTotalPages = totalPages;
    }

    public int getPageNo() {
        return mPageNo;
    }

    public void setPageNo(int pageNo) {
        mPageNo = pageNo;
    }

    public int getPerPage() {
        return mPerPage;
    }

    public void setPerPage(int perPage) {
        mPerPage = perPage;
    }

    public String getWebUrl() {
        return mWebUrl;
    }

    public void setWebUrl(String webUrl) {
        mWebUrl = webUrl;
    }
}

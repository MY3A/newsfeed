package com.creativewebmedia.newsfeed.data.api;

import com.creativewebmedia.newsfeed.data.model.NewsFeedWrapper;

import retrofit.Callback;
import retrofit.RestAdapter;

/**
 * Service class for interaction with server
 */
public class WebServiceManager {

    public static final String BASE_URL = "http://timesofindia.indiatimes.com/";

    /**
     * Creating instance of Retrofit RestAdapter
     */
    public static API getAppService() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        return restAdapter.create(API.class);
    }

    /**
     * Getting news feed
     *
     * @param callback callback for data processing
     */
    public static void getNewsFeed(Callback<NewsFeedWrapper> callback) {

        getAppService().getNewsFeed(callback);
    }
}
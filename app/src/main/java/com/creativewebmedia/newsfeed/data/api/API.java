package com.creativewebmedia.newsfeed.data.api;

import com.creativewebmedia.newsfeed.data.model.NewsFeedWrapper;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Class for providing REST-queries with Retrofit library
 */
public interface API {

    /**
     * Getting data for news feed
     *
     * @param cb - callback for data processing
     */
    @GET("/feeds/newsdefaultfeeds.cms?feedtype=sjson")
    void getNewsFeed(Callback<NewsFeedWrapper> cb);

}

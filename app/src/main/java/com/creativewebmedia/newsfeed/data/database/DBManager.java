package com.creativewebmedia.newsfeed.data.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.creativewebmedia.newsfeed.Utils.Constants;
import com.creativewebmedia.newsfeed.data.model.NewsItem;

import java.util.ArrayList;

/**
 * Class for managing database
 */
public class DBManager {

    private static final String DB_CREATE =
            "create table " + Constants.DB_TABLE + "(" +
                    Constants.COLUMN_ID + " long primary key, " +
                    Constants.COLUMN_IMG + " text, " +
                    Constants.COLUMN_TITLE + " text, " +
                    Constants.COLUMN_DATE + " text, " +
                    Constants.COLUMN_URL + " text" +
                    ");";

    private Context mCtx;
    private DBHelper mDBHelper;
    private SQLiteDatabase mDB;

    public DBManager(Context ctx) {
        mCtx = ctx;
    }

    /**
     * Initialization of DBHelper
     */
    public void open() {
        mDBHelper = new DBHelper(mCtx, Constants.DB_NAME, null, Constants.DB_VERSION);
        mDB = mDBHelper.getWritableDatabase();
    }

    /**
     * Closing DBHelper
     */
    public void close() {
        if (mDBHelper != null) mDBHelper.close();
    }

    /**
     * Getting news from local database
     */
    public Cursor getNewsFeed() {
        return mDB.rawQuery("select * from " + Constants.DB_TABLE, null);
    }

    /**
     * Writing news to local database
     *
     * @param newsFeed news feed items for inserting
     */
    public void insertNews(ArrayList<NewsItem> newsFeed) {

        for (NewsItem newsItem : newsFeed) {
            ContentValues cv = new ContentValues();
            cv.put(Constants.COLUMN_ID, newsItem.getId());
            cv.put(Constants.COLUMN_IMG, newsItem.getImage().getThumb());
            cv.put(Constants.COLUMN_TITLE, newsItem.getHeadline());
            cv.put(Constants.COLUMN_DATE, newsItem.getDate());
            cv.put(Constants.COLUMN_URL, newsItem.getUrl());
            mDB.insert(Constants.DB_TABLE, null, cv);
        }
    }

    /**
     * Clearing database before reloading data
     */
    public void clearData() {
        mDB.execSQL("delete from " + Constants.DB_TABLE);
    }

    /**
     * Class for database creating
     */
    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                        int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}

package com.creativewebmedia.newsfeed.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class for news item video information
 */
public class NewsVideo {

    @SerializedName("Thumb")
    String mThumb;

    @SerializedName("VideoCaption")
    String mVideoCaption;

    @SerializedName("DetailFeed")
    String mDetailFeed;

    @SerializedName("Type")
    String mType;

    public String getThumb() {
        return mThumb;
    }

    public void setThumb(String thumb) {
        mThumb = thumb;
    }

    public String getVideoCaption() {
        return mVideoCaption;
    }

    public void setVideoCaption(String videoCaption) {
        mVideoCaption = videoCaption;
    }

    public String getDetailFeed() {
        return mDetailFeed;
    }

    public void setDetailFeed(String detailFeed) {
        mDetailFeed = detailFeed;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }
}

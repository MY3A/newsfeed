package com.creativewebmedia.newsfeed.view;

import android.database.Cursor;

/**
 * View interface for news displaying
 */
public interface NewsView {

    void setItems(Cursor cursor);
    void showProgressDialog(boolean show);
    void showErrorDialog();
}

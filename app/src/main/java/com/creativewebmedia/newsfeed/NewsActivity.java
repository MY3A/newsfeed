package com.creativewebmedia.newsfeed;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.creativewebmedia.newsfeed.fragments.NewsFeedFragment;

/**
 * Launch activity of application
 */
public class NewsActivity extends AppCompatActivity {

    private ProgressDialog mProgressDialog;
    private ActionBar mActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        initProgressDialog();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mActionBar = getSupportActionBar();
        if (mActionBar != null)
            mActionBar.setDisplayShowHomeEnabled(true);

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                setHomeIsUp();
            }
        });

        Fragment retainedFragment = getSupportFragmentManager().findFragmentByTag("fragment");

        if (retainedFragment == null)
            showFragment(new NewsFeedFragment());
        else
            setHomeIsUp();

    }

    private void setHomeIsUp() {
        if (mActionBar != null) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                mActionBar.setDisplayHomeAsUpEnabled(true);
            } else {
                mActionBar.setDisplayHomeAsUpEnabled(false);
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    /**
     * Replacing fragment in activity
     *
     * @param fragment fragment for showing
     */
    public void showFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().
                replace(R.id.container, fragment, "fragment")
                .addToBackStack(null)
                .commit();
    }

    /**
     * Initialization of progress dialog
     */
    private void initProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getResources().getString(R.string.loading));
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    /**
     * Showing/hiding of progress dialog
     *
     * @param show flag for showing progress dialog
     */
    public void showProgressDialog(boolean show) {

        if (mProgressDialog != null)
            if (show)
                mProgressDialog.show();
            else
                mProgressDialog.hide();
    }

    /**
     * Showing dialog when error is occurred
     */
    public void showErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.error_message));
        builder.setTitle(getResources().getString(R.string.error_title));
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1)
            finish();
        else
            super.onBackPressed();
    }

}

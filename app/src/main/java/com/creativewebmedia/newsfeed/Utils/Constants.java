package com.creativewebmedia.newsfeed.Utils;

/**
 * Class for constants storing
 */
public class Constants {

    public static final String DB_NAME = "news_db";
    public static final int DB_VERSION = 1;
    public static final String DB_TABLE = "news";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_IMG = "thumb";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DATE = "dateline";
    public static final String COLUMN_URL = "url";

    public static final String[] FIELDS = new String[]{COLUMN_ID, COLUMN_IMG, COLUMN_TITLE, COLUMN_DATE, COLUMN_URL};

    public static final String CURRENT_FRAGMENT_NAME = "current_fragment_name";
}

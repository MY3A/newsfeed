package com.creativewebmedia.newsfeed.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.creativewebmedia.newsfeed.NewsActivity;
import com.creativewebmedia.newsfeed.R;
import com.creativewebmedia.newsfeed.adapters.SimpleNewsCursorAdapter;
import com.creativewebmedia.newsfeed.presenter.NewsPresenter;
import com.creativewebmedia.newsfeed.presenter.NewsPresenterImpl;
import com.creativewebmedia.newsfeed.view.NewsView;

/**
 * A fragment for news list displaying.
 */
public class NewsFeedFragment extends Fragment implements NewsView {

    private NewsActivity mActivity;
    private SimpleNewsCursorAdapter mNewsFeedAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private NewsPresenter mNewsPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (NewsActivity) getActivity();
        mNewsPresenter = new NewsPresenterImpl(this, getLoaderManager(), mActivity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news_feed, container, false);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout);
        RecyclerView newsFeed = (RecyclerView) view.findViewById(R.id.news_feed);
        newsFeed.setLayoutManager(new LinearLayoutManager(mActivity));
        mNewsFeedAdapter = new SimpleNewsCursorAdapter(mActivity, mNewsItemListener);
        newsFeed.setAdapter(mNewsFeedAdapter);

        mSwipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                mNewsPresenter.onReloadData();
            }
        });
        return view;
    }

    /**
     * News feed item click listener
     */
    SimpleNewsCursorAdapter.OnItemClickListener mNewsItemListener = new SimpleNewsCursorAdapter.OnItemClickListener() {

        @Override
        public void onItemClicked(String url) {
            mActivity.showFragment(NewsFragment.newInstance(url));
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        mNewsPresenter.onDestroy();
    }

    @Override
    public void setItems(Cursor cursor) {
        mNewsFeedAdapter.swapCursor(cursor);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showProgressDialog(boolean show) {
        mActivity.showProgressDialog(show);
    }

    @Override
    public void showErrorDialog() {
        mActivity.showErrorDialog();
    }
}

package com.creativewebmedia.newsfeed.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.creativewebmedia.newsfeed.R;

/**
 * Class for displaying single news.
 */
public class NewsFragment extends Fragment {

    private static final String ARG_URL = "url";

    private String mUrl;

    /**
     * Method for create a new instance of NewsFragment
     *
     * @param url URL for news displaying.
     * @return A new instance of fragment NewsFragment.
     */
    public static NewsFragment newInstance(String url) {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_URL, url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
            mUrl = getArguments().getString(ARG_URL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        WebView webView = (WebView) view.findViewById(R.id.news_view);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.loadUrl(mUrl);
        return view;
    }

}
